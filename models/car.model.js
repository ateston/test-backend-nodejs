const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const carSchema = new Schema({
  brand: {
    type: String,
    required: true
  },
  model: {
    type: String,
    required: true
  },
  year: {
    type: Number,
    required: true
  },
  color: {
    type: String,
  },
  vin: {
    type: String,
    required: true,
    unique: true,
    minlength: 17
  },
}, {
  timestamps: true,
});

const Car = mongoose.model('Car', carSchema);

module.exports = Car;