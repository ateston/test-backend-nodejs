const router = require('express').Router();
let Car = require('../models/car.model');

router.route('/').get((req, res) => {
  Car.find()
    .then(cars => res.json(cars))
    .catch(err => res.status(400).json('Error: ' + err));
});

router.route('/add').post((req, res) => {
  console.log(req.body);
  const newCar = new Car({
    brand: req.body.brand,
    model: req.body.model,
    year: req.body.year,
    color: req.body.color,
    vin: req.body.vin
  });

  newCar.save()
    .then(() => res.json('Car added with vin: ' + req.body.vin))
    .catch(err => res.status(400).json('Error: ' + err));
});

router.route('/:id').get((req, res) => {
  Car.findById(req.params.id)
    .then(car => res.json(car))
    .catch(err => res.status(400).json('Error: ' + err));
});

router.route('/:id').delete((req, res) => {
  Car.findByIdAndDelete(req.params.id)
    .then(() => res.json('Car with id: ' + req.params.id + ' deleted'))
    .catch(err => res.status(400).json('Error: ' + err));
});

module.exports = router;