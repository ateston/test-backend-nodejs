const cors = require('cors');
const express = require('express');
const mongoose = require('mongoose');

global.env = require('secure-env')({ secret: 'rokket' });
//require('dotenv').config();

const app = express();
const port = global.env.PORT || 5000;

app.use(cors());
app.use(express.json());

//mongo database connection
const mongo_uri = global.env.ATLASDB_URI;
mongoose.connect(mongo_uri, { useNewUrlParser: true, useCreateIndex: true });
const connection = mongoose.connection;

connection.once('open', () => {
  console.log("Mongo database connection established!");
});

//routes
const carsRouter = require('./routes/cars');
app.use('/cars', carsRouter);

app.listen(port, () => {
  console.log(`Server is running on port: ${port}`);
});